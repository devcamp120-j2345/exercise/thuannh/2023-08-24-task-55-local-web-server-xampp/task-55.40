package com.devcamp.task55_40.luckydice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LuckydiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LuckydiceApplication.class, args);
	}

}
