package com.devcamp.task55_40.luckydice.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
@RestController
@CrossOrigin
public class LuckyController {
    @GetMapping("/lucky")
    public String hello(@RequestParam String username, 
                        @RequestParam String firstname, 
                        @RequestParam String lastname) {
        int randomNum = (int) (Math.random() * 6 + 1);
        return "Xin chào " + username + ", Số may mắn hôm nay của bạn là: " + randomNum;
}
}
